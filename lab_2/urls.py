from django.urls import path
from .views import index_html, index_json, index_xml, index_json

urlpatterns = [
    path('', index_html, name='index_html'),
    path('xml/', index_xml, name='index_xml'),
    path('json/', index_json, name='index_json')
]
