from django.shortcuts import render, redirect
from django.http import response
from lab_2.models import Note
from .forms import NoteForm
# Create your views here.

def index(request):
    note = Note.objects.all().values()
    response = {'note': note}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    add_note = NoteForm(request.POST or None)
    if (add_note.is_valid() and request.method == 'POST'):
        add_note.save()
        return redirect('/lab-4')
    response = {'add_note':add_note}
    return render(request, 'lab4_form.html', response)

def note_list(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)
