Apakah perbedaan antara JSON dan XML?

XML merupakan bahasa markup, yaitu bahasa komputer yang menggunakan tags atau tanda. Tags tersebut akan dibaca oleh komputer dan diterjemahkan menjadi tampilan website. JSON merupakan format yang digunakan untuk menyimpan dan mentransfer data yang ditulis dalam JavaScript. Apabila membahas mengenai penyimpanan datanya, data XML disimpan sebagai tree structure. Sedangkan JSON menyimpan datanya seperti dictionary, yaitu map dengan pasangan key value.

XML dapat melakukan pemrosesan dan pemformatan dokumen serta objek, sehingga lambat dalam penguraian yang menyebabkan transmisi data menjadi lebih lambat. Kebalikan dari XML, JSON tidak melakukan pemrosesan atau perhitungan apapun, sehingga transfer data yang dilakukan lebih cepat karena ukuran file yang sangat kecil.

Apakah perbedaan antara HTML dan XML?

XML dan HTML merupakan script yang saling berkaitan, tetapi memiliki tujuan dan fungsi yang berbeda. Oleh karena itu, XML bukan merupakan pengganti HTML. XML lebih berfokus pada transfer data yang didorong konten dan menampilkan data sebenarnya, sedangkan HTML lebih fokus dalam penyajian data yang didorong format dan bagaimana agar data tersebut dapat terlihat.